<?php

namespace Drupal\commerce_product_bundles;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\commerce_product_bundles\service\ProductBundleVariationFieldRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProductBundleViewBuilder
 *
 * @package Drupal\commerce_product_bundles
 */
class ProductBundleViewBuilder extends EntityViewBuilder {

  /**
   * The product bundle field variation renderer.
   *
   * @var \Drupal\commerce_product_bundles\Service\ProductBundleVariationFieldRenderer
   */
  protected $bundleVariationFieldRenderer;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * ProductBundleViewBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   * @param \Drupal\commerce_product_bundles\service\ProductBundleVariationFieldRendererInterface $variation_field_renderer
   * @param \Drupal\Core\Entity\EntityRepository $entity_repository
   */
  public function __construct(EntityTypeInterface $entity_type, EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, ProductBundleVariationFieldRendererInterface $variation_field_renderer, EntityRepository $entity_repository) {
    parent::__construct($entity_type, $entity_manager, $language_manager);
    $this->bundleVariationFieldRenderer = $variation_field_renderer;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager'),
      $container->get('language_manager'),
      $container->get('commerce_product_bundles.bundle_variation_field_renderer'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    $product_bundle_type_storage = $this->entityManager->getStorage('commerce_product_bundles_type');

    /** @var \Drupal\commerce_product_bundles\ProductBundleVariationStorageInterface $bundle_variation_storage */
    $bundle_variation_storage = $this->entityManager->getStorage('commerce_bundle_variation');

    /** @var \Drupal\commerce_product_bundles\Entity\ProductBundleTypeInterface $product_bundle_type */
    $product_bundle_type = $product_bundle_type_storage->load($entity->bundle());

    if ($product_bundle_type->shouldInjectBundleVariationFields() && $entity->getDefaultVariation()) {
      $bundle_variation = $bundle_variation_storage->loadFromContext($entity);
      $bundle_variation = $this->entityRepository->getTranslationFromContext($bundle_variation, $entity->language()->getId());

      $rendered_fields = $this->bundleVariationFieldRenderer->renderFields($bundle_variation, $view_mode);
      foreach ($rendered_fields as $field_name => $rendered_field) {
        $build['variation_' . $field_name] = $rendered_field;
      }
    }
  }

}
