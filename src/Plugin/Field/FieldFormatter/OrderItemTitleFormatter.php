<?php

namespace Drupal\commerce_product_bundles\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'order_item_title_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "order_item_title_formatter",
 *   module = "commerce_product_bundles",
 *   label = @Translation("Order item title formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class OrderItemTitleFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * OrderItemTitleFormatter constructor.
   *
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings,
                              LanguageManagerInterface $language_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if (empty($items)) {
      return $elements;
    }

    foreach ($items as $delta => $item) {
      $order_item = $item->getEntity();
      $current_lng = $this->languageManager->getCurrentLanguage()->getId();

      switch ($order_item->bundle()){
        case 'bundle':
          // Construct purchased eck title.
          $bundle_variation = $order_item->getPurchasedEntity();
          $bundle_product = $bundle_variation->getBundleProduct();
          if($bundle_product->hasTranslation($current_lng)){
            $bundle_product->getTranslation($current_lng);
          }
          $order_item_title = $bundle_product->getTitle() . ' ' . $order_item->getTitle();
          $elements[$delta] = [
            '#markup' =>  '<div>' . $order_item_title . '</div>'
          ];

          break;
        default:
          $elements[$delta] = [
            '#markup' =>  $order_item->getTitle()
          ];
      }
    }

    return $elements;
  }

}
